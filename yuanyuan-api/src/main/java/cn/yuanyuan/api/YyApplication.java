package cn.yuanyuan.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author tao
 */
@SpringBootApplication
public class YyApplication {

    public static void main(String[] args) {
        SpringApplication.run(YyApplication.class, args);
    }

}
