package cn.yuanyuan.api.constant;

/**
 * @author tao
 */
public class MqInfo {

    public static final String DEFAULT_QUEUE = "default_queue";

    public static final String DEFAULT_EXCHANGE = "default_exchange";

    public static final String DIRECT_ROUTING = "direct_routing";


}
