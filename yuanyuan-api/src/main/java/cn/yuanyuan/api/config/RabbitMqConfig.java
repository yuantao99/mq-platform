package cn.yuanyuan.api.config;

import cn.yuanyuan.api.constant.MqInfo;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tao
 */
@Configuration
public class RabbitMqConfig {

    @Bean
    public Queue defaultQueue() {
        /* 队列名称: default_queue
         * durable 表示队列是否持久化
         * exclusive 表示队列是否仅声明队列的connection可以见。
         * ps: exclusive为真时，队列即使显示声明为durable，连接断开时（注意不是信道断开）也会被自动删除。
         * autoDelete表示是否在队列不再被使用是删除。
         * ps：对durable同exclusive一样影响。
         */
        return new Queue(MqInfo.DEFAULT_QUEUE, true, false, false);
    }

    @Bean
    public Exchange defaultExchange() {
        return new DirectExchange(MqInfo.DEFAULT_EXCHANGE, true, false);
    }

    @Bean
    public Binding defaultRouting(Exchange exchange, Queue queue) {
        return BindingBuilder.bind(queue).to(exchange).with(MqInfo.DIRECT_ROUTING).noargs();
    }

}
