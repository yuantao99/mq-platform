package cn.yuanyuan.api.controller;

import javax.annotation.Resource;

import cn.yuanyuan.api.provider.MessageHandleProvider;
import cn.yuanyuan.protocol.ApiResponse;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tao
 */
@RestController()
@RequestMapping("message")
@Slf4j
public class MessageController {

    @Resource
    MessageHandleProvider messageHandler;

    @PostMapping("/send")
    public ApiResponse<Void> sendMessage() {
        log.info("发送消息api");
        messageHandler.sendHello();
        return ApiResponse.success();
    }

}
