package cn.yuanyuan.api.provider;

import java.nio.charset.StandardCharsets;

import javax.annotation.Resource;

import cn.yuanyuan.api.constant.MqInfo;
import cn.yuanyuan.api.dmoain.SendResponse;
import lombok.extern.slf4j.Slf4j;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.stereotype.Component;

/**
 * @author tao
 */
@Component
@Slf4j
public class MessageHandleProvider {

    @Resource
    private AmqpTemplate amqpTemplate;

    public SendResponse sendHello() {
        log.info("将消息存放到消息队列中。");
        amqpTemplate.send(MqInfo.DEFAULT_EXCHANGE, MqInfo.DIRECT_ROUTING,
                MessageBuilder.withBody("hello👋".getBytes(StandardCharsets.UTF_8)).build());
        return new SendResponse();
    }

}
