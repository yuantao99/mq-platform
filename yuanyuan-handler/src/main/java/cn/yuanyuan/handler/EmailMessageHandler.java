package cn.yuanyuan.handler;

/**
 * @author tao
 */
public interface EmailMessageHandler {

    /**
     * 发送邮件
     * @param email     邮件地址
     * @param content   邮件内容
     */
    void send(String email, String content);

}
