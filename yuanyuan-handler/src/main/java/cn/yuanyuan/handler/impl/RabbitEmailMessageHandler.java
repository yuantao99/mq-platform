package cn.yuanyuan.handler.impl;

import cn.hutool.core.util.StrUtil;
import cn.yuanyuan.handler.EmailMessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author tao
 */
public class RabbitEmailMessageHandler implements EmailMessageHandler {

    private static final Logger log = LoggerFactory.getLogger(RabbitEmailMessageHandler.class);

    @Override
    public void send(String email, String content) {
        log.info(StrUtil.format("向{}：发送邮件内容{}", email, content));
    }

}
