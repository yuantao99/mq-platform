package cn.yuanyuan.handler.impl;

import cn.yuanyuan.handler.EmailMessageHandler;
import cn.yuanyuan.util.PersonMessageUtil;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author tao
 */
public class RabbitEmailMessageHandlerTest {

    private EmailMessageHandler emailMessageHandler;

    @BeforeEach
    void init() {
        emailMessageHandler = new RabbitEmailMessageHandler();
    }

    @Test
    void testSend() {
        emailMessageHandler.send(PersonMessageUtil.getAuthorEmail(), "你好呀👋！");
    }
}
