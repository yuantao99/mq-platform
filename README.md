# mq-platform

### 项目介绍

项目初期目标在于实战各消息队列软件的使用，参考了开源项目`austin`的项目需求，最终目的是完成一个消息推送平台。

#### 目录结构

1. `/gradlew`、`/gradlew.bat`、`/gradle/`为gradlew命令依赖，项目构建需要。
2. `/settings.gradle`为项目模块声明，各目录下的`build.gradle`为构建配置文件。
3. 模块`code-coverage-report`为模块代码覆盖率聚合模块。
4. `/devtools.sh`、`/docker-compose.yml`项目辅助启动工具。
5. 模块`init-db`为liquibase管理的数据库版本。

### 业务需求

#### 1.0.0

Task#01 实现一个响应发送邮件请求的api，响应请求的应用程序不做发送处理，利用消息队列进行异步处理发送邮件请求。

### 软件架构

![1.0.0-soft-architecture](./picture/1.0.0-soft-architecture.png)

### 相关命令

#### gradlew

1. 查看项目模块结构

```shell
./gradlew -q projects 
```

2. 代码测试覆盖率报告

```shell
./gradlew :code-coverage-report:aggregationCodeCoverageReport
```

3. 数据库表更新

```shell
cd init-db/
../gradlew :init-db:update
```

> 在code-coverage-report模块下生产的`build/jacocoReport/aggregationCodeCoverageReport/html/index.html`为项目的代码测试测试覆盖率报告。

#### docker-compose

项目使用docker-compose工具辅助本地开发的软件安装，`/docker-compose.yml`为配置文件。

> 为了docker-compose的顺利使用，需要确保机器已安转docker和docker-compose。
> 
> ps：配置文件中的镜像都为arm64架构。如果本地安装的docker内核为其他基础架构，如amd架构，则需要自行去docker hub下载对应版本。
> 
> 查看docker的基础架构为：docker version。Server信息中的Engine:OS/Arch属性表明本地安装docker的系统指令架构。

1. 安装rabbitmq

```shell
docker-compose -f ./docker-compose.yml up -d yuanyuan-rabbitmq
```

> 安装完成后可以访问本地端口5678，进入rabbitmq的管理界面。

2. 安装mysql

```shell
docker-compose -f ./docker-compose.yml up -d yuanyuan-mysql
```

> 映射端口3041。

2. 安装adminer

```shell
docker-compose -f ./docker-compose.yml up -d yuanyuan-db-adminer
```

> 映射端口8041，进入页面后服务器(server)填写yuanyuan-mysql。

#### devtools.sh

为了方便使用，提供了本地开发的脚本工具。命令行中，在根目录运行`./devtools.sh`。根据提示既可以安装所需的软件。

例如，安装rabbitmq：

```text
mq-platform # ./devtools.sh
Please select what you want to do:
1) read_me
2) setup_rabbitmq
3) setup_mysql
4) init_db
#? 2
Creating rabbitmq ... done
finish exc install rabbitmq.
```

### 进度

- [x] 使用gradle构建springboot多模块项目。
- [x] 添加测试框架junit 5和assertj断言工具，并集成测试覆盖率工具javacoco。
- [x] 添加日志框架slf4j+logback。
- [x] 添加json处理工具jackson和工具集hutool、guava，添加lombok。
- [x] 添加第一个业务需求。
- [x] 添加docker-compose.yml，编写rabbitmq的安装配置。使用docker-compose安装rabbitmq。
- [x] 添加一个sh脚本，简化docker-compose的使用。
- [x] common模块添加统一响应协议类。
- [x] 添加spring mvc，编写一个接口且服务正常启动后可以调用。
- [x] 集成amqp、 rabbitmq，并向消息队列发送一个消息。
- [x] docker-compose添加mysql、adminer的安装配置，并集成liquibase，添加消息模版等表。
- [x] sh脚本添加mysql环境的安装脚本。

### BUG记录

1# @Slf4j，gradle构建无法解析log符号。

gradle配置编译支持lombok注解，`annotationProcessor "org.projectlombok:lombok:${lombokVersion}"`

2# org.springframework.web.HttpMediaTypeNotAcceptableException: Could not find acceptable representation.

在springboot返回给前端数据内容时，如果内容是实体类需要转为json返回去，这里需要实体类有getter和setter方法，所以错误原因就是由于没有给返回给前端的实体类写getter/setter方法导致，加上后不报错。
