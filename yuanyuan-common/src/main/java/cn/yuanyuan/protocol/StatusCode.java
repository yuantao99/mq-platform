package cn.yuanyuan.protocol;

/**
 * 响应状态接口
 *
 * @author tao
 */
public interface StatusCode {

    /**
     * 状态码
     * @return 状态码
     */
    int getCode();

    /**
     * 状态码描述
     * @return 转台码描述
     */
    String getMessage();

}
