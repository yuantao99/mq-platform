package cn.yuanyuan.protocol;

import lombok.Getter;

/**
 * api响应码
 *
 * @author tao
 */
public enum ApiCode implements StatusCode{
    /**
     * api请求响应正常。
     */
    SUCCESS(10200, "响应成功"),

    /**
     * api服务端发送错误。
     */
    SERVER_ERROR(10500, "服务器报错"),
    ;

    private final int code;
    private final String message;

    ApiCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
