package cn.yuanyuan.protocol;

/**
 * api响应结果
 *
 * @author tao
 */
public class ApiResponse<T> extends BaseResponse<T>{

    public ApiResponse(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ApiResponse(StatusCode statusCode, T data) {
        this(statusCode.getCode(), statusCode.getMessage(), data);
    }

    public ApiResponse(StatusCode statusCode) {
        this(statusCode, null);
    }

    public static <K> ApiResponse<K> success(K data) {
        return new ApiResponse<>(ApiCode.SUCCESS, data);
    }

    public static  ApiResponse<Void> success() {
        return new ApiResponse<>(ApiCode.SUCCESS);
    }

}
