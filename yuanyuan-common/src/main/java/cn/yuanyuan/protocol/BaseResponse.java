package cn.yuanyuan.protocol;

import lombok.Getter;
import lombok.Setter;

/**
 * 各类响应超类
 *
 * @author tao
 */
@Getter
@Setter
public abstract class BaseResponse<T> {

    /**
     * 状态码
     */
    protected int code;

    /**
     * 状态码信息
     */
    protected String message;

    /**
     * 返回对象
     */
    protected T data;

}
