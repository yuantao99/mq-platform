package cn.yuanyuan.util;

import cn.hutool.core.lang.Assert;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author tao
 */
public class PersonMessageUtilTest {

    @Test
    void testGetPersonEmail() {
        String email = PersonMessageUtil.getAuthorEmail();
        assertThat(email).isEqualTo("13060889537@sina.cn");
    }

}
